COMPLIANT_BRANCHES=(
"1.0.x"
"2.0.x"
"3.0.x"
"2.1.x"
"deploy"
"master"
)
COMPLIANT=0
for branch in "${COMPLIANT_BRANCHES[@]}"
do
    if [ $BITBUCKET_BRANCH == $branch ]; then
        COMPLIANT=1
    fi
done

if [ $COMPLIANT == 0 ]; then
  echo "$BITBUCKET_BRANCH cannot publish"
  exit 1
fi