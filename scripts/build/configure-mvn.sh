#!/bin/bash

# See: https://extranet.atlassian.com/display/BBCI/questions/2662794344/how-can-i-build-a-java-project-in-bitbucket-ci-that-relies-on-closedsource-parent-pom

mvn --version

MAVEN_SETTINGS="$(mvn --version | sed -n -E 's/Maven home: (.*)/\1/p')/conf/settings.xml"

if [ ! -f "$MAVEN_SETTINGS" ]; then
   echo "Maven not installed, or maven settings is in a different place on this docker image! Not found at '$MAVEN_SETTINGS'"
   exit 1
fi

echo "Updating configuration in Maven settings file: '$MAVEN_SETTINGS'"

function server {
    sed -i'bak' '/<servers>/ a\
             <server><id>'"$1"'</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>
             ' "$MAVEN_SETTINGS"
}

# Add username and password for committer of scm server on for maven tasks https://ecosystem.atlassian.net/browse/AMKT-24439
# More info: https://maven.apache.org/scm/maven-scm-providers/maven-scm-providers-git/maven-scm-provider-jgit/

function scm-server {
    sed -i'bak' '/<servers>/ a\
             <server><id>'"$1"'</id><username>${GIT_USER_NAME}</username><password>${MP_DEV_BOT_2SV_PWD}</password></server>
             ' "$MAVEN_SETTINGS"
}

function repositories {
    sed -i'bak' '/<profiles>/ a\
             <profile><id>'"$1"'</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>'"$1"'</id><name>'"$1"'</name><url>'"$2"'</url><layout>default</layout></repository></repositories><pluginRepositories><pluginRepository><id>'"$1"'</id><url>'"$2"'</url></pluginRepository></pluginRepositories></profile>
             ' "$MAVEN_SETTINGS"
}

server "atlassian-m2-repository"
server "atlassian-m2-snapshot-repository"
server "maven-atlassian-com"
server "atlassian-private"
server "atlassian-private-snapshot"
server "atlassian-public"
server "atlassian-public-snapshot"
server "atlassian-3rdparty"
server "atlassian-3rdparty-snapshot"
server "atlassian-restricted"
server "atlassian-central"
server "atlassian-central-snapshot"
scm-server "atlassian-mpac-client-bitbucket"

repositories "atlassian-public" "https://packages.atlassian.com/maven/repository/public"
repositories "maven-atlassian-com" "https://packages.atlassian.com/maven/repository/internal"
