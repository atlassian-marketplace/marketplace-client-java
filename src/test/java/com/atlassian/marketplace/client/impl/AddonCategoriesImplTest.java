package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.AddonCategorySummary;
import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.net.URI;
import java.util.Arrays;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_APPLICATIONS_PATH;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonCategorySummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.application;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.verify;

public class AddonCategoriesImplTest extends ApiImplTestBase
{
    private static final String FAKE_APP_BY_KEY_PATH = "/fake/applications/";
    private static final String FAKE_CATEGORIES_BY_APP_PATH = "/fake/categories/";
    private static final AddonCategorySummary CAT_REP = addonCategorySummary().build();
    private static final Links APPS_LINKS =
        ModelBuilders.links().putTemplate("byKey", FAKE_APP_BY_KEY_PATH + "{applicationKey}")
                             .build();
    private static final InternalModel.Applications APPS_REP =
            InternalModel.applications(APPS_LINKS, Arrays.asList());
    private static final Application APP_REP =
            application().addLink("addonCategories", URI.create(FAKE_CATEGORIES_BY_APP_PATH)).build();
    private static final InternalModel.AddonCategories CATS_REP =
            InternalModel.addonCategories(links().build(), Arrays.asList(CAT_REP));
    
    @Test
    public void categoriesAccessorQueriesRootResource() throws Exception
    {
        tester.client.addonCategories();
        
        verify(tester.httpTransport).get(tester.apiBase);
    }
    
    @Test(expected=MpacException.class)
    public void categoriesAccessorThrowsExceptionIfRootResourceNotAvailable() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 404);
        tester.client.addonCategories();
    }

    @Test
    public void getByApplicationUsesAppByKeyResource() throws Exception
    {
        URI appsApiZeroLengthQuery = UriBuilder.fromUri(HOST_BASE + FAKE_APPLICATIONS_PATH).queryParam("limit", 0).build();
        URI appQuery = UriBuilder.fromUri(HOST_BASE + FAKE_APP_BY_KEY_PATH + JIRA.getKey()).build();
        URI catsQuery = UriBuilder.fromUri(HOST_BASE + FAKE_CATEGORIES_BY_APP_PATH).build();
        tester.mockResource(appsApiZeroLengthQuery, APPS_REP);
        tester.mockResource(appQuery, APP_REP);
        tester.mockResource(catsQuery, CATS_REP);

        assertThat(tester.client.addonCategories().findForApplication(JIRA), contains(CAT_REP));
    }
}
