package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.api.Cost;
import com.atlassian.marketplace.client.api.HostingType;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.ProductQuery;
import com.atlassian.marketplace.client.api.ProductVersionSpecifier;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.Product;
import com.atlassian.marketplace.client.model.ProductVersion;
import com.atlassian.marketplace.client.util.UriBuilder;
import org.junit.Test;

import java.net.URI;
import java.util.Arrays;
import java.util.Optional;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.TestObjects.LINKS_WITH_NEXT;
import static com.atlassian.marketplace.client.TestObjects.LINKS_WITH_PREV;
import static com.atlassian.marketplace.client.TestObjects.LINK_NEXT_URI;
import static com.atlassian.marketplace.client.TestObjects.LINK_PREV_URI;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_PRODUCTS_PATH;
import static com.atlassian.marketplace.client.model.TestModelBuilders.product;
import static com.atlassian.marketplace.client.model.TestModelBuilders.productVersion;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;

public class ProductsImplTest extends ApiImplTestBase
{
    private static final String FAKE_PRODUCT_BY_KEY_PATH = "/fake/products/by/key/";
    private static final Product PRODUCT_REP = product().build();
    private static final ProductVersion PRODUCT_VER_REP = productVersion().build();
    private static final Links PRODUCTS_LINKS = ModelBuilders.links()
            .putTemplate("byKey", FAKE_PRODUCT_BY_KEY_PATH + "{productKey}")
            .putTemplate("latestVersion", FAKE_PRODUCT_BY_KEY_PATH + "{productKey}/latest")
            .putTemplate("versionByBuild", FAKE_PRODUCT_BY_KEY_PATH + "{productKey}/build/{buildNumber}")
            .putTemplate("versionByName", FAKE_PRODUCT_BY_KEY_PATH + "{productKey}/name/{versionName}")
            .build();
    private static final InternalModel.Products PRODUCTS_REP =
            InternalModel.products(PRODUCTS_LINKS, Arrays.asList(PRODUCT_REP), 2);
    private static final InternalModel.Products PRODUCTS_REP_WITH_NEXT =
            InternalModel.products(LINKS_WITH_NEXT, Arrays.asList(PRODUCT_REP), 2);
    private static final InternalModel.Products PRODUCTS_REP_WITH_PREV =
            InternalModel.products(LINKS_WITH_PREV, Arrays.asList(PRODUCT_REP), 2);
    
    @Test
    public void getByKeyUsesProductsResource() throws Exception
    {
        setupProductByKeyResource(productByKeyUri("x"));
        tester.client.products().safeGetByKey("x", ProductQuery.any());

        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_PRODUCTS_PATH + "?limit=0"));
    }

    @Test
    public void getByKeyUsesProductByKeyResource() throws Exception
    {
        setupProductByKeyResource(productByKeyUri("x"));

        assertThat(tester.client.products().safeGetByKey("x", ProductQuery.any()),
                equalTo(Optional.of(PRODUCT_REP)));
    }

    @Test
    public void getByKeyPassesAppKeyInQueryString() throws Exception
    {
        setupProductByKeyResource(productByKeyUri("x").queryParam("application", "jira"));
        
        assertThat(tester.client.products().safeGetByKey("x", ProductQuery.builder().application(Optional.of(JIRA)).build()),
                equalTo(Optional.of(PRODUCT_REP)));
    }

    @Test
    public void getByKeyPassesAppBuildNumberInQueryString() throws Exception
    {
        setupProductByKeyResource(productByKeyUri("x").queryParam("application", "jira").queryParam("applicationBuild", "123"));
        
        assertThat(tester.client.products().safeGetByKey("x", ProductQuery.builder().application(Optional.of(JIRA)).appBuildNumber(Optional.of(123)).build()),
                   equalTo(Optional.of(PRODUCT_REP)));
    }

    @Test
    public void getByKeyPassesVersionFlagInQueryString() throws Exception
    {
        setupProductByKeyResource(productByKeyUri("x").queryParam("withVersion", "true"));
        
        assertThat(tester.client.products().safeGetByKey("x", ProductQuery.builder().withVersion(true).build()),
                   equalTo(Optional.of(PRODUCT_REP)));
    }

    @Test
    public void getVersionUsesProductsResource() throws Exception
    {
        setupProductVersionResource(latestVersionUri("x"));
        tester.client.products().safeGetVersion("x", ProductVersionSpecifier.latest());

        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_PRODUCTS_PATH + "?limit=0"));
    }

    @Test
    public void getLatestVersionUsesLatestVersionResource() throws Exception
    {
        setupProductVersionResource(latestVersionUri("x"));

        assertThat(tester.client.products().safeGetVersion("x", ProductVersionSpecifier.latest()),
                equalTo(Optional.of(PRODUCT_VER_REP)));
    }

    @Test
    public void getVersionByBuildUsesVersionByBuildResource() throws Exception
    {
        setupProductVersionResource(versionByBuildUri("x", 100));

        assertThat(tester.client.products().safeGetVersion("x", ProductVersionSpecifier.buildNumber(100)),
                equalTo(Optional.of(PRODUCT_VER_REP)));
    }

    @Test
    public void getVersionByBuildUsesVersionByNameResource() throws Exception
    {
        setupProductVersionResource(versionByNameUri("x", "1.0"));

        assertThat(tester.client.products().safeGetVersion("x", ProductVersionSpecifier.name("1.0")),
                equalTo(Optional.of(PRODUCT_VER_REP)));
    }

    @Test
    public void findUsesProductsResource() throws Exception
    {
        setupProductsResource(productsApi());
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()), contains(PRODUCT_REP));
    }

    @Test
    public void findReturnsPageWithCorrectSize() throws Exception
    {
        setupProductsResource(productsApi());
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()).size(), equalTo(1));
    }

    @Test
    public void findReturnsPageWithCorrectTotalSize() throws Exception
    {
        setupProductsResource(productsApi());
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()).totalSize(), equalTo(2));
    }

    @Test
    public void findReturnsPageWithReference() throws Exception
    {
        QueryBounds b = QueryBounds.limit(Optional.of(5));
        UriBuilder uri = productsApi().queryParam("limit", 5);
        setupProductsResource(uri, PRODUCTS_REP);
        
        assertThat(tester.client.products().find(ProductQuery.builder().bounds(b).build()).safeGetReference(),
                   equalTo(Optional.of(new PageReference<Product>(uri.build(), b, PageReader.<Product>stub()))));
    }
    
    @Test
    public void findReturnsPageWithNoNextReferenceIfNoNextLink() throws Exception
    {
        setupProductsResource(productsApi(), PRODUCTS_REP_WITH_PREV);
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()).safeGetNext(),
                   equalTo(Optional.empty()));
    }

    @Test
    public void findReturnsPageWithNextReferenceIfNextLink() throws Exception
    {
        QueryBounds b = QueryBounds.limit(Optional.of(5));
        UriBuilder uri = productsApi().queryParam("limit", 5);
        setupProductsResource(uri, PRODUCTS_REP_WITH_NEXT);
        
        assertThat(tester.client.products().find(ProductQuery.builder().bounds(b).build()).safeGetNext(),
                   equalTo(Optional.of(new PageReference<Product>(LINK_NEXT_URI, b.withOffset(5), PageReader.<Product>stub()))));
    }

    @Test
    public void findReturnsPageWithNextReferenceUsingSizeAsLimitIfLimitOmitted() throws Exception
    {
        setupProductsResource(productsApi(), PRODUCTS_REP_WITH_NEXT);
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()).safeGetNext(),
                   equalTo(Optional.of(new PageReference<Product>(LINK_NEXT_URI,
                                                                  QueryBounds.offset(1).withLimit(Optional.of(1)), PageReader.<Product>stub()
                   ))));
    }

    @Test
    public void findReturnsPageWithNoPrevReferenceIfNoPrevLink() throws Exception
    {
        setupProductsResource(productsApi(), PRODUCTS_REP_WITH_NEXT);
        
        assertThat(tester.client.products().find(ProductQuery.builder().build()).safeGetPrevious(),
                   equalTo(Optional.empty()));
    }

    @Test
    public void findReturnsPageWithPrevReferenceIfPrevLink() throws Exception
    {
        QueryBounds b = QueryBounds.offset(8).withLimit(Optional.of(5));
        UriBuilder uri = productsApi().queryParam("offset", 8).queryParam("limit", 5);
        setupProductsResource(uri, PRODUCTS_REP_WITH_PREV);
        
        assertThat(tester.client.products().find(ProductQuery.builder().bounds(b).build()).safeGetPrevious(),
                   equalTo(Optional.of(new PageReference<Product>(LINK_PREV_URI, b.withOffset(3), PageReader.<Product>stub()))));
    }

    @Test
    public void findPassesOffsetInQueryString() throws Exception
    {
        QueryBounds b = QueryBounds.offset(5);
        setupProductsResource(productsApi().queryParam("offset", "5"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().bounds(b).build()), contains(PRODUCT_REP));
    }

    @Test
    public void findPassesLimitInQueryString() throws Exception
    {
        QueryBounds b = QueryBounds.limit(Optional.of(10));
        setupProductsResource(productsApi().queryParam("limit", "10"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().bounds(b).build()), contains(PRODUCT_REP));
    }

    @Test
    public void findPassesAppKeyInQueryString() throws Exception
    {
        setupProductsResource(productsApi().queryParam("application", "jira"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().application(Optional.of(JIRA)).build()), contains(PRODUCT_REP));
    }

    @Test
    public void findPassesAppBuildNumberInQueryString() throws Exception
    {
        setupProductsResource(productsApi().queryParam("application", "jira").queryParam("applicationBuild", "123"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().application(Optional.of(JIRA)).appBuildNumber(Optional.of(123)).build()),
                   contains(PRODUCT_REP));
    }

    @Test
    public void findPassesCostInQueryString() throws Exception
    {
        setupProductsResource(productsApi().queryParam("cost", "paid"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().cost(Optional.of(Cost.ALL_PAID)).build()),
                   contains(PRODUCT_REP));
    }

    @Test
    public void findPassesVersionFlagInQueryString() throws Exception
    {
        setupProductsResource(productsApi().queryParam("withVersion", "true"));
        
        assertThat(tester.client.products().find(ProductQuery.builder().withVersion(true).build()),
                   contains(PRODUCT_REP));
    }

    @Test
    public void findPassesHostingInQueryString() throws Exception
    {
        setupProductsResource(productsApi().queryParam("hosting", "cloud"));

        assertThat(tester.client.products().find(ProductQuery.builder().hosting(Optional.of(HostingType.CLOUD)).build()),
                contains(PRODUCT_REP));
    }

    private UriBuilder productsApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCTS_PATH);
    }

    private UriBuilder productsApiZeroLengthQuery()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCTS_PATH).queryParam("limit", 0);
    }

    private UriBuilder productByKeyUri(String key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCT_BY_KEY_PATH + key);
    }

    private UriBuilder latestVersionUri(String key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCT_BY_KEY_PATH + key + "/latest");
    }

    private UriBuilder versionByBuildUri(String key, int build)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCT_BY_KEY_PATH + key + "/build/" + build);
    }

    private UriBuilder versionByNameUri(String key, String name)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_PRODUCT_BY_KEY_PATH + key + "/name/" + name);
    }

    private void setupProductsResource(UriBuilder productsUri) throws Exception
    {
        setupProductsResource(productsUri, PRODUCTS_REP);
    }
    
    private void setupProductsResource(UriBuilder productsUri, InternalModel.Products rep) throws Exception
    {
        tester.mockResource(productsUri.build(), rep);
    }
    
    private void setupProductByKeyResource(UriBuilder uri) throws Exception
    {
        setupProductsResource(productsApiZeroLengthQuery());
        tester.mockResource(uri.build(), PRODUCT_REP);
    }
    
    private void setupProductVersionResource(UriBuilder uri) throws Exception
    {
        setupProductsResource(productsApiZeroLengthQuery());
        tester.mockResource(uri.build(), PRODUCT_VER_REP);
    }
}
