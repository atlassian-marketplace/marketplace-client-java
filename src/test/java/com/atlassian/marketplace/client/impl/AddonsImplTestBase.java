package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonReference;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionSummary;
import com.atlassian.marketplace.client.model.Entity;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableList;

import java.net.URI;
import java.util.Arrays;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.TestObjects.LINKS_WITH_NEXT;
import static com.atlassian.marketplace.client.TestObjects.LINKS_WITH_PREV;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_ADDONS_PATH;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addon;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonReference;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonVersion;

public abstract class AddonsImplTestBase<T extends Entity> extends ApiImplTestBaseWithCreateAndUpdate<T>
{
    protected static final String FAKE_ADDON_BY_KEY_PATH = "/fake/addons/by/key/";
    protected static final String FAKE_ADDON_VERSIONS_PATH = FAKE_ADDON_BY_KEY_PATH + "versions";
    protected static final String FAKE_ADDON_VERSION_BY_BUILD_PATH = FAKE_ADDON_VERSIONS_PATH + "/build/";
    protected static final String FAKE_ADDON_VERSION_BY_NAME_PATH = FAKE_ADDON_VERSIONS_PATH + "/name/";
    protected static final String FAKE_TOKENS_PATH = FAKE_ADDON_BY_KEY_PATH + "tokens";
    protected static final String FAKE_LATEST_VERSION_PATH = FAKE_ADDON_VERSIONS_PATH + "/latest";
    protected static final String FAKE_PRICING_TEMPLATE = "/fake/pricing/{cloudOrServer}/{liveOrPending}";
    protected static final String FAKE_BANNERS_PATH = "/fake/banners";
    protected static final String FAKE_RECOMMENDATIONS_PATH = "/fake/recommendations";
    protected static final Links ADDONS_LINKS = links()
            .putTemplate("byKey", FAKE_ADDON_BY_KEY_PATH + "{addonKey}")
            .putTemplate("banners", FAKE_BANNERS_PATH + "{?whatever}")
            .build();
    protected static final Links ADDON_VERSIONS_LINKS = links()
            .putTemplate("byBuild", FAKE_ADDON_VERSION_BY_BUILD_PATH + "{pluginBuildNumber}")
            .putTemplate("byName", FAKE_ADDON_VERSION_BY_NAME_PATH + "{name}")
            .put("latest", URI.create(FAKE_LATEST_VERSION_PATH))
            .build();
    protected static final Links TOKENS_LINKS = links()
            .putTemplate("byToken", FAKE_TOKENS_PATH + "/{token}")
            .build();
    protected static final Links ADDON_LINKS = links()
            .put("self", URI.create(FAKE_ADDON_BY_KEY_PATH))
            .putTemplate("pricing", FAKE_PRICING_TEMPLATE)
            .put("recommendations", URI.create(FAKE_RECOMMENDATIONS_PATH))
            .put("tokens", URI.create(FAKE_TOKENS_PATH))
            .put("versions", URI.create(FAKE_ADDON_VERSIONS_PATH))
            .build();
    protected static final Addon ADDON_REP = addon().links(ADDON_LINKS).build();
    protected static final AddonSummary ADDON_SUMMARY_REP = addonSummary().build();
    protected static final AddonReference ADDON_REF_REP = addonReference().build();
    protected static final AddonVersion ADDON_VERSION_REP = addonVersion().build();
    protected static final InternalModel.Addons ADDONS_REP =
            InternalModel.addons(ADDONS_LINKS, Arrays.asList(ADDON_SUMMARY_REP), 2);
    protected static final InternalModel.Addons ADDONS_REP_WITH_NEXT =
            InternalModel.addons(LINKS_WITH_NEXT, Arrays.asList(ADDON_SUMMARY_REP), 2);
    protected static final InternalModel.Addons ADDONS_REP_WITH_PREV =
            InternalModel.addons(LINKS_WITH_PREV, Arrays.asList(ADDON_SUMMARY_REP), 2);
    protected static final InternalModel.AddonVersions ADDON_VERSIONS_REP =
            InternalModel.addonVersions(ADDON_VERSIONS_LINKS, Arrays.asList(), 0);
    protected static final InternalModel.AddonReferences BANNERS_REP =
            InternalModel.addonReferences(links().build(), Arrays.asList(ADDON_REF_REP), 2);
    protected static final InternalModel.AddonReferences RECOMMENDATIONS_REP =
            InternalModel.addonReferences(links().build(), Arrays.asList(ADDON_REF_REP), 2);
    protected static final InternalModel.MinimalLinks TOKENS_REP =
            new InternalModel.MinimalLinks(TOKENS_LINKS);
    
    protected UriBuilder addonsApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDONS_PATH);
    }

    protected UriBuilder addonsApiZeroLengthQuery()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDONS_PATH).queryParam("limit", 0);
    }

    protected UriBuilder addonByKeyUri(String key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_BY_KEY_PATH + key);
    }

    protected UriBuilder addonVersionZeroLengthQuery()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_VERSIONS_PATH).queryParam("limit", 0);
    }

    protected UriBuilder latestVersionUri(String key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_LATEST_VERSION_PATH);
    }

    protected UriBuilder recommendationsUri()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_RECOMMENDATIONS_PATH);
    }

    protected UriBuilder tokensUri()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_TOKENS_PATH);
    }
    
    protected URI tokenUri(String token)
    {
        return URI.create(HOST_BASE + FAKE_TOKENS_PATH + "/" + token);
    }

    protected UriBuilder versionByBuildUri(long buildNumber)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_VERSION_BY_BUILD_PATH + + buildNumber);
    }

    protected UriBuilder versionByNameUri(String name)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_VERSION_BY_NAME_PATH + name);
    }
    
    protected UriBuilder bannersApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_BANNERS_PATH);
    }
    
    protected void setupAddonsResource(UriBuilder addonsUri) throws Exception
    {
        setupAddonsResource(addonsUri, ADDONS_REP);
    }
    
    protected void setupAddonsResource(UriBuilder addonsUri, InternalModel.Addons rep) throws Exception
    {
        tester.mockResource(addonsUri.build(), rep);
    }

    protected void setupAddonBannersResource(UriBuilder addonsUri) throws Exception
    {
        setupAddonBannersResource(addonsUri, BANNERS_REP);
    }
    
    protected void setupAddonBannersResource(UriBuilder addonsUri, InternalModel.AddonReferences rep) throws Exception
    {
        setupAddonsResource(addonsApiZeroLengthQuery());
        tester.mockResource(addonsUri.build(), rep);
    }

    protected void setupAddonByKeyResource(UriBuilder uri) throws Exception
    {
        setupAddonsResource(addonsApiZeroLengthQuery());
        tester.mockResource(uri.build(), ADDON_REP);
    }

    protected void setupAddonByKeyResourceError(UriBuilder uri, int status) throws Exception
    {
        setupAddonsResource(addonsApiZeroLengthQuery());
        tester.mockResourceError(uri.build(), status);
    }

    protected void setupVersionResource(String addonKey, UriBuilder uri) throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri(addonKey));
        tester.mockResource(addonVersionZeroLengthQuery().build(), ADDON_VERSIONS_REP);
        tester.mockResource(uri.build(), ADDON_VERSION_REP);
    }
    
    protected void setupRecommendationsResource(String addonKey, UriBuilder uri) throws Exception
    {
        setupAddonsResource(addonsApiZeroLengthQuery());
        setupAddonByKeyResource(addonByKeyUri(addonKey));
        tester.mockResource(uri.build(), RECOMMENDATIONS_REP);
    }

    protected void setupClaimTokenResource(String addonKey, String token, int status) throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri(addonKey).queryParam("accessToken", token));
        tester.mockResource(tokensUri().queryParam("accessToken", token).queryParam("limit", "0").build(),
            TOKENS_REP);
        tester.mockPostResource(tokenUri("t"), status);
    }

    protected void setupClaimTokenAddonByKeyResourceError(String addonKey, String token, int status) throws Exception
    {
        setupAddonByKeyResourceError(addonByKeyUri(addonKey).queryParam("accessToken", token), status);
    }
}
