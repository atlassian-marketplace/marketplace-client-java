package com.atlassian.marketplace.client.impl;

import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DateTimeJsonTest extends BaseJsonTests
{
    @Test
    public void dateTimeIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"2014-05-24T18:00:00.000Z\"}", HasDateTime.class).x,
                equalTo(ZonedDateTime.of(2014, 05, 24, 18, 00, 00, 000, ZoneOffset.UTC)));
    }

    @Test
    public void localDateIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"2014-05-24\"}", HasLocalDate.class).x,
            equalTo(LocalDate.of(2014, 05, 24)));
    }
    
    @Test
    public void localDateIsEncoded() throws Exception
    {
        HasLocalDate o = new HasLocalDate();
        o.x = LocalDate.of(2014, 05, 24);
        assertThat(encode(o), equalTo("{\"x\":\"2014-05-24\"}"));
    }
    
    static final class HasDateTime
    {
        ZonedDateTime x;
    }
    
    static final class HasLocalDate
    {
        LocalDate x;
    }
}
