package com.atlassian.marketplace.client.http;

import com.atlassian.marketplace.client.http.HttpConfiguration.Credentials;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthMethod;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthParams;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyConfiguration;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyHost;
import com.atlassian.marketplace.client.impl.CommonsHttpTransport;
import com.atlassian.utt.http.TestHttpServer.RequestProperties;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.net.URI;

import static com.atlassian.utt.http.TestHttpServer.BASIC_AUTH_CHALLENGE;
import static com.atlassian.utt.http.TestHttpServer.DIGEST_AUTH_CHALLENGE;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.method;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestHasNoProxyAuth;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestHasProxyAuth;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestHasProxyBasicAuth;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestHasProxyDigestAuth;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.uri;
import static com.atlassian.utt.http.TestHttpServer.proxyAuthChallenge;
import static com.atlassian.utt.http.TestHttpServer.requests;
import static com.atlassian.utt.http.TestHttpServer.status;
import static io.atlassian.fugue.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class CommonsHttpTransportProxyTest extends CommonsHttpTransportTestBase
{
    private static final int IMAGINARY_UNDERLYING_SERVER_PORT = 8111;
    private static final URI IMAGINARY_UNDERLYING_SERVER_URI =
        URI.create("http://localhost:" + IMAGINARY_UNDERLYING_SERVER_PORT);
    private static final URI ABSOLUTE_TEST_URI = IMAGINARY_UNDERLYING_SERVER_URI.resolve(TEST_URI);
    
    private static final String AUTH_USERNAME = "user";
    private static final String AUTH_PASSWORD = "pass";
    private static final String AUTH_HASH = "dXNlcjpwYXNz";
    
    @Test
    public void unauthedProxyRequest() throws Exception
    {
        http = makeHttp(baseProxyConfig());
        resp = http.get(ABSOLUTE_TEST_URI);
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("GET")),
                    uri().is(equalTo(ABSOLUTE_TEST_URI)),
                    requestHasNoProxyAuth()
                )
            )
        ));
    }

    @Test
    public void basicAuthProxyRequestUsesPreemptiveAuth() throws Exception
    {
        http = makeHttp(basicAuthProxyConfig());
        resp = http.get(ABSOLUTE_TEST_URI);
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("GET")),
                    uri().is(equalTo(ABSOLUTE_TEST_URI)),
                    requestHasProxyBasicAuth(AUTH_HASH)
                )
            )
        ));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void digestAuthProxyRequestRespondsToChallenge() throws Exception
    {
        http = makeHttp(digestAuthProxyConfig());
        server.respondIf(
            requestHasNoProxyAuth(),
            proxyAuthChallenge(DIGEST_AUTH_CHALLENGE)
        );
        server.respondIf(
            requestHasProxyAuth(),
            status(200).body("ok")
        );
        resp = http.get(ABSOLUTE_TEST_URI);
        assertThat(server, requests().is(Matchers.<RequestProperties>contains(
            requestHasNoProxyAuth(),
            requestHasProxyDigestAuth()
        )));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void multiAuthProxyRequestRespondsToChallenge() throws Exception
    {
        http = makeHttp(digestAuthProxyConfig());
        server.respondIf(
            requestHasNoProxyAuth(),
            status(407)
                .header("Proxy-Authenticate", "Negotiate")
                .header("Proxy-Authenticate", DIGEST_AUTH_CHALLENGE)
                .header("Proxy-Authenticate", BASIC_AUTH_CHALLENGE)
        );
        server.respondIf(
            requestHasProxyAuth(),
            status(200).body("ok")
        );
        resp = http.get(ABSOLUTE_TEST_URI);
        assertThat(server, requests().is(Matchers.<RequestProperties>contains(
            requestHasNoProxyAuth(),
            requestHasProxyDigestAuth()
        )));
    }

    private CommonsHttpTransport makeHttp(ProxyConfiguration.Builder builder)
    {
        return new CommonsHttpTransport(HttpConfiguration.builder()
            .proxyConfiguration(some(builder.build())).build(), IMAGINARY_UNDERLYING_SERVER_URI);
    }
    
    private ProxyConfiguration.Builder baseProxyConfig()
    {
        return ProxyConfiguration.builder()
            .proxyHost(some(new ProxyHost("localhost", serverPort)));
    }
    
    private ProxyConfiguration.Builder basicAuthProxyConfig()
    {
        ProxyAuthParams proxyAuth =
            new ProxyAuthParams(new Credentials(AUTH_USERNAME, AUTH_PASSWORD), ProxyAuthMethod.BASIC);

        return baseProxyConfig().authParams(some(proxyAuth));
    }

    private ProxyConfiguration.Builder digestAuthProxyConfig()
    {
        ProxyAuthParams proxyAuth =
            new ProxyAuthParams(new Credentials(AUTH_USERNAME, AUTH_PASSWORD), ProxyAuthMethod.DIGEST);

        return baseProxyConfig().authParams(some(proxyAuth));
    }
}
