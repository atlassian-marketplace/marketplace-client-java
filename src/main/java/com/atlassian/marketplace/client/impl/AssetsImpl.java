package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.api.Assets;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.ImagePurpose;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Optional;

final class AssetsImpl extends ApiImplBase implements Assets
{
    AssetsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "assets");
    }

    @Override
    public ArtifactId uploadAddonArtifact(File artifactFile) throws MpacException
    {
        URI uri = apiHelper.requireLinkUri(getLinksOnly(getApiRoot()), "artifact", Assets.class);
        return ArtifactId.fromUri(uploadAssetFromFile(uri, artifactFile, Optional.empty()));
    }

    @Override
    public ArtifactId uploadAddonArtifact(File artifactFile, String pluginKey) throws MpacException
    {
        URI uri = apiHelper.requireLinkUri(getLinksOnly(getApiRoot()), "artifact", Assets.class);
        return ArtifactId.fromUri(uploadAssetFromFile(uri, artifactFile, Optional.of(pluginKey)));
    }

    @Override
    public ImageId uploadImage(File imageFile, ImagePurpose imageType) throws MpacException
    {
        UriTemplate ut = ApiHelper.requireLinkUriTemplate(getLinksOnly(getApiRoot()), "imageByType", Assets.class);
        URI uri = apiHelper.resolveLink(ut.resolve(ImmutableMap.of("imageType", imageType.getKey())));
        return ImageId.fromUri(uploadAssetFromFile(uri, imageFile, Optional.empty()));
    }
    
    private URI uploadAssetFromFile(URI collectionUri, File file, Optional<String> pluginKey) throws MpacException
    {
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                return uploadAssetFromStream(collectionUri, fis, file.length(), file.getName(), pluginKey);
            }
            finally
            {
                fis.close();
            }
        }
        catch (IOException e)
        {
            throw new MpacException(e);
        }
    }
    
    private URI uploadAssetFromStream(URI collectionUri,
                                      InputStream stream,
                                      long length,
                                      String logicalFileName,
                                      final Optional<String> pluginKey) throws MpacException
    {
        UriBuilder uriBuilder = UriBuilder.fromUri(collectionUri).queryParam("file", logicalFileName);
        pluginKey.map(key -> uriBuilder.queryParam("pluginKey", key));
        URI uri = uriBuilder.build();

        InternalModel.MinimalLinks result =
            apiHelper.postContent(uri, stream, length, "application/binary", InternalModel.MinimalLinks.class);
        return ApiHelper.requireLink(result.getLinks(), "self", Links.class).getUri();
    }
}
