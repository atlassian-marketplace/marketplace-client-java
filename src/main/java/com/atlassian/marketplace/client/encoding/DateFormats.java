package com.atlassian.marketplace.client.encoding;

import java.time.ZoneOffset;

/**
 * Defines the format strings used for date and date/time values in the Marketplace REST API.
 * @since 2.0.0
 */
public abstract class DateFormats
{
    private DateFormats()
    {
    }
    
    /**
     * The format used for all string representations of dates that do not have time values or time zones.
     */
    public static java.time.format.DateTimeFormatter DATE_FORMATTER = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * The format used for all string representations of dates that also have time values.
     */
    public static java.time.format.DateTimeFormatter DATE_TIME_FORMATTER = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneOffset.UTC);

}
