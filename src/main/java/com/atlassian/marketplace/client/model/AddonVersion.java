package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.AddonVersionExternalLinkType;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.HostingType;
import com.atlassian.marketplace.client.api.LicenseTypeId;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import io.atlassian.fugue.Option;

import java.net.URI;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.StreamSupport;

import static com.google.common.collect.Iterables.transform;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.option;
import static io.atlassian.fugue.Option.some;

/**
 * Information about a specific version of an {@link Addon}.
 * @see com.atlassian.marketplace.client.api.Addons
 * @see AddonVersionSummary
 * @since 2.0.0
 */
public final class AddonVersion extends AddonVersionBase
{
    Embedded _embedded;
    Long buildNumber;
    transient Option<Long> dataCenterBuildNumber;
    Option<String> youtubeId;
    Option<ImmutableList<VersionCompatibility>> compatibilities;
    TextProperties text;
    @ReadOnly Option<LegacyProperties> legacy;

    @Override
    public Option<ArtifactInfo> getArtifactInfo()
    {
        return _embedded.artifact;
    }

    @Override
    public Option<URI> getArtifactUri()
    {
        for (ArtifactInfo a: _embedded.artifact)
        {
            return some(a.getBinaryUri());
        }
        return none();
    }

    @Override
    public Option<URI> getRemoteDescriptorUri()
    {
        for (ArtifactInfo a: _embedded.artifact)
        {
            return a.getRemoteDescriptorUri();
        }
        return none();
    }

    /**
     * The version's build number, a value specified by the vendor (or, in the case of Atlassian
     * Connect add-ons, set automatically by Marketplace) that distinguishes it from all other versions
     * of the add-on and determines the correct ordering of versions.
     * @see ModelBuilders.AddonVersionBuilder#buildNumber
     */
    public long getBuildNumber()
    {
        return buildNumber;
    }

    public Option<Long> getDataCenterBuildNumber() {
        return dataCenterBuildNumber;
    }
    
    /**
     * A list of {@link VersionCompatibility} objects representing each of the
     * {@link Application}s the add-on version is compatible with, and the compatible
     * application version range.  For an existing add-on listing, there will always be
     * at least one of these.
     * @see #getCompatibilitiesIfSpecified()
     */
    public Iterable<VersionCompatibility> getCompatibilities()
    {
        return compatibilities.getOrElse(ImmutableList.<VersionCompatibility>of());
    }
    
    /**
     * Same as {@link #getCompatibilities}, but returns {@code none()} rather than an empty
     * list if this is a new version that you are creating which has no compatibilities specified
     * (meaning that it will inherit its compatibilities from the previous version).
     */
    public Option<Iterable<VersionCompatibility>> getCompatibilitiesIfSpecified()
    {
        return compatibilities.map(Function.identity());
    }
    
    @Override
    public Option<URI> getExternalLinkUri(AddonVersionExternalLinkType type)
    {
        if (type.canSetForNewAddonVersions())
        {
            return option(vendorLinks.get(type.getKey()));
        }
        else
        {
            for (LegacyProperties l: legacy)
            {
                return option(l.vendorLinks.get(type.getKey()));
            }
            return none();
        }
    }
    
    @Override
    public Iterable<AddonCategorySummary> getFunctionalCategories()
    {
        return _embedded.functionalCategories;
    }

    /**
     * The image/text "highlight" items that are featured in a public add-on listing; may be
     * omitted for private versions.
     * @see #getHighlightsIfSpecified()
     * @see ModelBuilders.AddonVersionBuilder#highlights(Iterable)
     */
    public Iterable<Highlight> getHighlights()
    {
        return _embedded.highlights.getOrElse(ImmutableList.of());
    }
    
    /**
     * Same as {@link #getHighlights}, but returns {@code none()} rather than an empty
     * list if this is a new version that you are creating which has no highlights specified
     * (meaning that it will inherit its highlights from the previous version).
     */
    public Option<Iterable<Highlight>> getHighlightsIfSpecified()
    {
        return _embedded.highlights.map(Function.identity());
    }

    /**
     * Information about the add-on version's software license type; may be omitted for
     * private versions.
     * @see ModelBuilders.AddonVersionBuilder#licenseType(Option)
     * @see #getLicenseTypeId()
     */
    public Option<LicenseType> getLicenseType()
    {
        return _embedded.license;
    }
    
    /**
     * The unique resource identifier for the add-on version's software license type; may be
     * omitted for private versions.
     * @see ModelBuilders.AddonVersionBuilder#licenseTypeId(Option)
     * @see #getLicenseType()
     */
    public Option<LicenseTypeId> getLicenseTypeId()
    {
        for (URI u: getLinks().getUri("license"))
        {
            return some(LicenseTypeId.fromUri(u));
        }
        return none();
    }
    
    /**
     * Optional descriptive text to be displayed in the add-on listing.
     * @see ModelBuilders.AddonVersionBuilder#moreDetails(Option)
     */
    public Option<HtmlString> getMoreDetails()
    {
        return text.moreDetails;
    }

    @Override
    public PaymentModel getPaymentModel()
    {
        return paymentModel;
    }

    /**
     * Optional details about this release of the add-on (for instance, new features).
     * @see ModelBuilders.AddonVersionBuilder#releaseNotes(Option)
     */
    public Option<HtmlString> getReleaseNotes()
    {
        return text.releaseNotes;
    }

    /**
     * A brief description of the nature or purpose of this release (for instance, "bug fixes").
     * @see ModelBuilders.AddonVersionBuilder#releaseSummary(Option)
     */
    public Option<String> getReleaseSummary()
    {
        return text.releaseSummary;
    }
    
    /**
     * Screenshot images with optional captions.
     * @see #getScreenshotsIfSpecified()
     * @see ModelBuilders.AddonVersionBuilder#screenshots(Iterable)
     */
    public Iterable<Screenshot> getScreenshots()
    {
        return _embedded.screenshots.getOrElse(ImmutableList.of());
    }

    /**
     * Same as {@link #getScreenshots}, but returns {@code none()} rather than an empty
     * list if this is a new version that you are creating which has no screenshots specified
     * (meaning that it will inherit its screenshots from the previous version).
     */
    public Option<Iterable<Screenshot>> getScreenshotsIfSpecified()
    {
        return _embedded.screenshots.map(Function.identity());
    }

    @Override
    public AddonVersionStatus getStatus()
    {
        return status;
    }

    @Override
    public boolean isStatic()
    {
        return staticAddon;
    }
    
    /**
     * The string key of a YouTube video to be featured in the add-on listing.
     * @see ModelBuilders.AddonVersionBuilder#youtubeId(Option)
     */
    public Option<String> getYoutubeId()
    {
        return youtubeId;
    }

    /**
     * Returns just the application keys from the list of compatibilities.
     * @see #getCompatibilities()
     */
    public Iterable<ApplicationKey> getCompatibleApplications()
    {
        return transform(getCompatibilities(), VersionCompatibility::getApplication);
    }
    
    /**
     * Shortcut for testing whether the version has any compatibility with the specified application.
     * @see #getCompatibilities()
     */
    public boolean isCompatibleWithApplication(ApplicationKey application)
    {
        for (VersionCompatibility c: getCompatibilities())
        {
            if (c.getApplication().equals(application))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Shortcut for testing whether the version is compatible with the specified application
     * build and hosting type.
     * @see #getCompatibilities()
     */
    public boolean isCompatibleWith(java.util.function.Predicate<ApplicationKey> applicationCriteria, HostingType hostingType, int build)
    {
        return StreamSupport.stream(getCompatibilities().spliterator(), false)
                .anyMatch(c -> c.isCompatibleWith(applicationCriteria, hostingType, build));
    }

    static final class Embedded
    {
        Option<ArtifactInfo> artifact;
        ImmutableList<AddonCategorySummary> functionalCategories;
        Option<ImmutableList<Highlight>> highlights;
        Option<LicenseType> license;
        Option<ImmutableList<Screenshot>> screenshots;
    }

    static final class LegacyProperties
    {
        Map<String, URI> vendorLinks;
    }
    
    static final class TextProperties
    {
        Option<String> releaseSummary;
        Option<HtmlString> moreDetails;
        Option<HtmlString> releaseNotes;
    }
}
