package com.atlassian.marketplace.client.model;

import com.google.common.collect.ImmutableList;
import io.atlassian.fugue.Option;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.function.Function;

/**
 * Information about the pricing of a paid-via-Atlassian add-on.
 * @since 2.0.0
 */
public class AddonPricing
{
    @ReadOnly Links _links;
    ImmutableList<AddonPricingItem> items;
    Option<ImmutableList<AddonPricingItem>> perUnitItems;
    Boolean expertDiscountOptOut;
    Boolean contactSalesForAdditionalPricing;
    Option<String> parent;

    @ReadOnly
    ZonedDateTime lastModified;

    Option<RoleInfo> role;

    public Links getLinks()
    {
        return _links;
    }

    /**
     * A list of available pricing tiers.
     */
    public Iterable<AddonPricingItem> getItems()
    {
        return items;
    }

    /**
     * A list of available per unit pricing brackets.
     */
    public Iterable<AddonPricingItem> getPerUnitItems()
    {
        return perUnitItems.getOrElse( ImmutableList.of() );
    }

    /**
     * Same as {@link #getPerUnitItems}, but returns {@code none()} rather than an empty
     * list if per unit pricing is not set.
     */
    public Option<Iterable<AddonPricingItem>> getPerUnitItemsIfSpecified()
    {
        return perUnitItems.map(Function.identity());
    }

    /**
     * True if Atlassian Experts <i>cannot</i> purchase the add-on at a discount.
     */
    public boolean isExpertDiscountOptOut()
    {
        return expertDiscountOptOut;
    }

    /**
     * True if the vendor can be contacted for additional pricing.
     */
    public boolean isContactSalesForAdditionalPricing()
    {
        return contactSalesForAdditionalPricing;
    }

    /**
     * Key of the application that the add-on is for (e.g. "jira").
     */
    public Option<String> getParent()
    {
        return parent;
    }

    /**
     * Timestamp of the last change made to this pricing.
     */
    public Optional<ZonedDateTime> getLastModifiedZonedDate()
    {
        return Optional.ofNullable(lastModified);
    }

    /**
     * Returns true if the pricing is role-based, false if not (e.g. user-tiered).
     */
    public boolean isRoleBased()
    {
        return getRoleInfo().isDefined();
    }

    /**
     * Additional information describing the pricing role for a role-based plugin.
     * <p>
     * Will only be set for role-based plugins; user-tiered plugins will not have any value set.
     */
    public Option<RoleInfo> getRoleInfo()
    {
        return role;
    }

    /**
     * Additional information describing the pricing role for a role-based plugin.
     */
    public static class RoleInfo
    {
        String singularName;
        String pluralName;
        
        /**
         * The singular form of the pricing role name, such as "user".
         * <p>
         * The name should be in American English.
         */
        public String getSingularName()
        {
            return singularName;
        }

        /**
         * The plural form of the pricing role name, such as "users".
         * <p>
         * The name should be in American English.
         */
        public String getPluralName()
        {
            return pluralName;
        }
    }
}
