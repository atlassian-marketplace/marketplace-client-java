package com.atlassian.marketplace.client.model;

import io.atlassian.fugue.Option;

import static io.atlassian.fugue.Option.none;

public final class VendorPrograms
{

    Option<TopVendorProgram> topVendor = none();

    public Option<TopVendorProgram> getTopVendor()
    {
        return topVendor;
    }
}
