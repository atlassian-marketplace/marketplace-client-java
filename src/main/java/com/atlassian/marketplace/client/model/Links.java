package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.UriTemplate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Option;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.option;
import static io.atlassian.fugue.Option.some;

/**
 * A links map in the HAL-JSON format used by the Marketplace version 2 API.  For each link rel (key),
 * there can be either a single URI, a single URI template, or an array of either.  Example of a
 * valid links map:
 * <code>
 *   {
 *     "simple": { "href": "/uri" },
 *     "multiple": [ { "href": "/uri1" }, { "href": "/uri2" } ],
 *     "withContentType": { "href": "/uri", "type": "text/html" },
 *     "isATemplate": { "href": "/uri/{var}", "templated": true }
 *   }
 * </code>
 */
public final class Links
{
    public static final String REST_TYPE = "application/json";
    public static final String WEB_TYPE = "text/html";

    private final Map<String, List<Link>> items;

    /**
     * @param items the items to be processed
     */
    public Links(Map<String, List<Link>> items)
    {
        this.items = ImmutableMap.copyOf(items);
    }

    public Map<String, List<Link>> getItemsMap()
    {
        Map<String, List<Link>> linksMap = new HashMap<>();
        for (Map.Entry<String, List<Link>> entry : items.entrySet())
        {
            linksMap.put(entry.getKey(), new ArrayList<>(entry.getValue()));
        }
        return linksMap;
    }

    URI requireUri(String rel)
    {
        for (URI uri: getUri(rel))
        {
            return uri;
        }
        throw new IllegalArgumentException("missing required REST link: " + rel);
    }
    
    /**
     * Returns the first {@link Link} matching the given key, if any.
     */
    public Option<Link> getLink(String rel)
    {
        for (Link link: getLinks(rel))
        {
            return some(link);
        }
        return none();
    }

    /**
     * Returns the first {@link Link} matching the given key and the given content type, if any.
     */
    public Option<Link> getLink(String rel, String contentType)
    {
        for (Link link: getLinks(rel))
        {
            if (link.getType().isEmpty() && contentType.equals(REST_TYPE))
            {
                return some(link);
            }
            for (String type: link.getType())
            {
                if (type.equalsIgnoreCase(contentType))
                {
                    return some(link);
                }
            }
        }
        return none();
    }

    /**
     * Returns every {@link Link} that matches the given key.
     */
    public Iterable<Link> getLinks(String rel)
    {
        return option(items.get(rel)).getOrElse(ImmutableList.of());
    }

    /**
     * Shortcut for getting the URI of the first {@link Link} that matches the given key.
     */
    public Option<URI> getUri(String rel)
    {
        for (Link link: getLink(rel))
        {
            return some(link.getUri());
        }
        return none();
    }

    /**
     * Shortcut for getting the URI of the first {@link Link} that matches the given key and content type.
     */
    public Option<URI> getUri(String rel, String contentType)
    {
        for (Link link: getLink(rel, contentType))
        {
            return some(link.getUri());
        }
        return none();
    }

    /**
     * Shortcut for getting the template of the first {@link Link} that matches the given key,
     * if it is a template.
     */
    public Option<UriTemplate> getUriTemplate(String rel)
    {
        for (Link link: getLink(rel))
        {
            return link.getUriTemplate();
        }
        return none();
    }
    
    /**
     * Shortcut for getting the template of the first {@link Link} that matches the given key
     * and content type, if it is a template.
     */
    public Option<UriTemplate> getUriTemplate(String rel, String contentType)
    {
        for (Link link: getLink(rel, contentType))
        {
            return link.getUriTemplate();
        }
        return none();
    }
}
