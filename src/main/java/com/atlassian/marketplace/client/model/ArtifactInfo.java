package com.atlassian.marketplace.client.model;

import io.atlassian.fugue.Option;

import java.net.URI;

/**
 * Information about an installable file hosted by Marketplace.
 * @since 2.0.0
 */
public final class ArtifactInfo implements Entity
{
    Links _links;
    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "binary") URI binaryUri;
    
    /**
     * The URI from which the actual file can be downloaded. This is different from
     * {@link #getSelfUri()}, which is the URI of the asset resource (see
     * {@link com.atlassian.marketplace.client.api.Assets}).
     */
    public URI getBinaryUri()
    {
        return binaryUri;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }

    /**
     * The remote URI of the descriptor where it is hosted by its application, if this artifact
     * is the descriptor of an Atlassian Connect add-on.
     */
    public Option<URI> getRemoteDescriptorUri()
    {
        return _links.getUri("remote");
    }
    
    @Override
    public Links getLinks()
    {
        return _links;
    }
}
