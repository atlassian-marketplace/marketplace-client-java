package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonCategoryId;
import com.atlassian.marketplace.client.api.AddonExternalLinkType;
import com.atlassian.marketplace.client.api.AddonVersionExternalLinkType;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.LicenseTypeId;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.api.VendorExternalLinkType;
import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.encoding.SchemaViolation;
import com.atlassian.marketplace.client.impl.SchemaViolationException;
import com.atlassian.marketplace.client.model.Screenshot.ScreenshotEmbedded;
import com.atlassian.marketplace.client.model.VersionCompatibility.CompatibilityHosting;
import com.atlassian.marketplace.client.model.VersionCompatibility.CompatibilityHostingBounds;
import com.atlassian.marketplace.client.util.Convert;
import com.atlassian.marketplace.client.util.EntityFunctions;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Option;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

import static com.atlassian.marketplace.client.api.ResourceId.resourceIdToUriFunc;
import static com.atlassian.marketplace.client.impl.EntityValidator.validateInstance;
import static com.atlassian.marketplace.client.model.TestModelBuilders.DEFAULT_URI;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonDistributionSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonReviewsSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.imageInfo;
import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.option;
import static io.atlassian.fugue.Option.some;
import static java.util.stream.Collectors.toList;

/**
 * Classes for constructing model objects for the 2.0 API.  Although model objects are usually
 * returned to you by API methods, you may want to construct them yourself in order to create or
 * update them on the server (such as creating a new add-on).  You should only ever construct
 * these objects using the builders provided here; do not try to use their constructors directly.
 * <p>
 * The general pattern for using these builders is: 1. call a builder factory method, such as
 * {@link #addon()}, which returns a builder of the appropriate type; 2. call property setter
 * methods on the builder, such as {@link AddonBuilder#name(String)}; 3. after setting all the
 * desired properties, call the builder's {@code build()} method, which returns the finished
 * object.  Note that the builder factory methods can also be given an existing object as a
 * parameter, in which case they initialize the builder by copying all of its properties from
 * the existing object.
 * <p>
 * If you have omitted any required property, {@code build()} will throw an
 * {@link InvalidModelException}.  (You can easily identify required properties because they
 * do not use the {@code Option} type; properties in this library are never nullable, so if a
 * property is a {@code String} rather than an {@code Option<String>}, it must have a value.)
 * Optional properties always default to {@link Option#none()} if you do not specify a value.
 * <p>
 * For some very simple objects, the factory methods construct and return a finished object
 * rather than returning a builder.
 */
public abstract class ModelBuilders
{
    private ModelBuilders()
    {
    }
    
    /**
     * {@link ModelBuilders} methods that declare this checked exception will throw it if you attempt
     * to construct a model object that does not conform to the Marketplace API schema, for instance
     * by omitting a required property.
     * @since 2.0.0
     */
    @SuppressWarnings("serial")
    public static class InvalidModelException extends MpacException
    {
        private ImmutableList<SchemaViolation> schemaViolations;
        
        InvalidModelException(Iterable<SchemaViolation> schemaViolations)
        {
            this.schemaViolations = ImmutableList.copyOf(schemaViolations);
        }
        
        public Iterable<SchemaViolation> getSchemaViolations()
        {
            return schemaViolations;
        }
        
        @Override
        public String getMessage()
        {
            return Joiner.on(", ").join(schemaViolations);
        }
    }
    
    /**
     * Common interface for builders that will always succeed in building an object;
     * any required properties that you do not set will be given default values.
     * @param <T>  the type of the object being built
     * @since 2.0.0
     */
    public interface SafeBuilder<T>
    {
        T build();
    }
    
    /**
     * Common interface for builders that do not provide any default values for
     * required properties, and will throw an exception if you try to call {@code build()}
     * before you have set all the required properties.
     * @param <T>  the type of the object being built
     * @since 2.0.0
     */
    public interface UnsafeBuilder<T>
    {
        T build() throws InvalidModelException;
    }

    private static InvalidModelException modelException(SchemaViolationException e)
    {
        return new InvalidModelException(e.getSchemaViolations());
    }
    /**
     * Creates a new {@link AddonBuilder}.
     * @since 2.0.0
     */
    public static AddonBuilder addon()
    {
        return new AddonBuilder();
    }

    /**
     * Creates a new {@link AddonBuilder} initialized from the properties of an existing {@link Addon}.
     * @since 2.0.0
     */
    public static AddonBuilder addon(Addon from)
    {
        return new AddonBuilder(from);
    }

    /**
     * Creates a new {@link AddonVersionBuilder}.
     * @since 2.0.0
     */
    public static AddonVersionBuilder addonVersion()
    {
        return new AddonVersionBuilder();
    }

    /**
     * Creates a new {@link AddonVersionBuilder} initialized from the properties of an existing {@link AddonVersion}.
     * @since 2.0.0
     */
    public static AddonVersionBuilder addonVersion(AddonVersion from)
    {
        return new AddonVersionBuilder(from);
    }
    
    /**
     * Creates a new {@link AddressBuilder}.
     * @since 2.0.0
     */
    public static AddressBuilder address()
    {
        return new AddressBuilder();
    }
    
    /**
     * Creates a new {@link ApplicationVersionBuilder}.
     * @since 2.0.0
     */
    public static ApplicationVersionBuilder applicationVersion()
    {
        return new ApplicationVersionBuilder();
    }
    
    /**
     * Creates a new {@link ApplicationVersionBuilder} initialized from the properties of an existing {@link ApplicationVersion}.
     * @since 2.0.0
     */
    public static ApplicationVersionBuilder applicationVersion(ApplicationVersion from)
    {
        return new ApplicationVersionBuilder(from);
    }
    
    /**
     * Creates a new {@link HighlightBuilder}.
     * @since 2.0.0
     */
    public static HighlightBuilder highlight()
    {
        return new HighlightBuilder();
    }

    /**
     * Creates a new {@link LinksBuilder}.
     * @since 2.0.0
     */
    public static LinksBuilder links()
    {
        return new LinksBuilder();
    }

    /**
     * Creates a new {@link ScreenshotBuilder}.
     * @since 2.0.0
     */
    public static ScreenshotBuilder screenshot()
    {
        return new ScreenshotBuilder();
    }
    
    /**
     * Creates a new {@link VendorBuilder}.
     * @since 2.0.0
     */
    public static VendorBuilder vendor()
    {
        return new VendorBuilder();
    }

    /**
     * Creates a new {@link VendorBuilder} initialized from the properties of an existing {@link Vendor}.
     * @since 2.0.0
     */
    public static VendorBuilder vendor(Vendor from)
    {
        return new VendorBuilder(from);
    }

    public static VersionCompatibilityBuilder versionCompatibility(VersionCompatibility from) {
        return new VersionCompatibilityBuilder(from);
    }
    
    /**
     * Creates a {@link VersionCompatibility} that specifies compatibility with an application in
     * Cloud hosting only.  For Cloud hosting, there is no application version range.
     * @since 2.0.0
     *
     * @param appKey the key of the application we are describing compatibility with
     * @return a VersionCompatibility instance indicating only Cloud compatibility
     */
    public static VersionCompatibility versionCompatibilityForCloud(ApplicationKey appKey)
    {
        return new VersionCompatibility(appKey,
            new CompatibilityHosting(none(CompatibilityHostingBounds.class), none(CompatibilityHostingBounds.class), some(true)));
    }

    /**
     * Creates a {@link VersionCompatibility} that specifies compatibility with an application in
     * Server hosting only.  For Server hosting, you must specify the application version range as
     * a minimum and maximum build number (this value comes from {@link ApplicationVersion#getBuildNumber()}).
     * @since 2.0.0
     *
     * @param appKey the key of the application we are describing compatibility with
     * @param minBuild the minimum build number of the application with which we are compatible
     * @param maxBuild the maximum build number of the application with which we are compatible
     * @return a VersionCompatibility instance indicating the specified compatibility, and not Cloud compatible
     */
    public static VersionCompatibility versionCompatibilityForServer(ApplicationKey appKey,
                                                                     int minBuild, int maxBuild)
    {
        return new VersionCompatibility(appKey,
                new CompatibilityHosting(some(makeHostingBounds(minBuild, maxBuild)), none(CompatibilityHostingBounds.class), some(false)));
    }

    /**
     * Creates a {@link VersionCompatibility} that specifies compatibility with an application in
     * Data Center hosting.  For Data Center hosting, you must specify the application version ranges as
     * minimum and maximum build numbers (this value comes from {@link ApplicationVersion#getBuildNumber()}).
     * @since 2.0.0
     *
     * @param appKey the key of the application we are describing compatibility with.
     * @param minBuild the minimum build number of the application with which we are compatible.
     * @param maxBuild the maximum build number of the application with which we are compatible.
     * @return a VersionCompatibility instance indicating the specified compatibility, and not Server and Cloud compatible
     */
    public static VersionCompatibility versionCompatibilityForDataCenter(ApplicationKey appKey,
                                                                         int minBuild,
                                                                         int maxBuild) {
        return new VersionCompatibility(appKey,
                new CompatibilityHosting(none(CompatibilityHostingBounds.class),
                        some(makeHostingBounds(minBuild, maxBuild)),
                        some(false)));
    }

    /**
     * Creates a {@link VersionCompatibility} that specifies compatibility with an application in
     * Server and Data Center hosting.  For Server and Data Center hosting, you must specify the application version ranges as
     * minimum and maximum build numbers (this value comes from {@link ApplicationVersion#getBuildNumber()}).
     * @since 2.0.0
     *
     * @param appKey the key of the application we are describing compatibility with
     * @param minBuild the minimum build number of the application with which we are compatible
     * @param maxBuild the maximum build number of the application with which we are compatible
     * @param dataCenterMinBuild the minimum build number of the application running as DC with which we are compatible
     * @param dataCenterMaxBuild the maximum build number of the application running as DC with which we are compatible
     * @return a VersionCompatibility instance indicating the specified compatibility, and not Cloud compatible
     */
    public static VersionCompatibility versionCompatibilityForServerAndDataCenter(ApplicationKey appKey,
                                                                                  int minBuild,
                                                                                  int maxBuild,
                                                                                  int dataCenterMinBuild,
                                                                                  int dataCenterMaxBuild) {
        return new VersionCompatibility(appKey,
                                        new CompatibilityHosting(some(makeHostingBounds(minBuild, maxBuild)),
                                                                 some(makeHostingBounds(dataCenterMinBuild,
                                                                                        dataCenterMaxBuild)),
                                                                 some(false)));
    }

    /**
     * Creates a {@link VersionCompatibility} that specifies compatibility with an application in both
     * Cloud and Server hosting, specifying the application version range for Server hosting as
     * a minimum and maximum build number (this value comes from {@link ApplicationVersion#getBuildNumber()}).
     * @since 2.0.0
     *
     * @param appKey the key of the application we are describing compatibility with
     * @param minBuild the minimum build number of the application with which we are compatible
     * @param maxBuild the maximum build number of the application with which we are compatible
     * @return a VersionCompatibility instance indicating the specified compatibility, including that it is Cloud compatible
     */
    public static VersionCompatibility versionCompatibilityForServerAndCloud(ApplicationKey appKey,
                                                                             int minBuild, int maxBuild)
    {
        return new VersionCompatibility(appKey,
            new CompatibilityHosting(some(makeHostingBounds(minBuild, maxBuild)), none(CompatibilityHostingBounds.class), some(true)));
    }

    private static CompatibilityHostingBounds makeHostingBounds(int min, int max)
    {
        return new CompatibilityHostingBounds(
            new VersionCompatibility.VersionPoint(min, none(String.class)),
            new VersionCompatibility.VersionPoint(max, none(String.class)));
    }
    
    public static abstract class BuilderWithLinks<T extends BuilderWithLinks<T>>
    {
        protected final LinksBuilder links = new LinksBuilder();
     
        protected BuilderWithLinks()
        {
            // All model entities must have a "self" link.  This link is always generated by the server and
            // is ignored in any object that is posted to the server; we provide a default for it here so
            // newly created objects will never fail validation due to the lack of it.
            links.put("self", TestModelBuilders.DEFAULT_URI);
        }
     
        @SuppressWarnings("unchecked")
        public T links(Links links)
        {
            this.links.removeAll();
            this.links.put(links);
            return (T) this;
        }
        
        @SuppressWarnings("unchecked")
        public T addLinks(Links links)
        {
            this.links.put(links);
            return (T) this;
        }
        
        @SuppressWarnings("unchecked")
        public T addLink(String rel, URI uri)
        {
            this.links.put(rel, uri);
            return (T) this;
        }
        
        @SuppressWarnings("unchecked")
        public T addLink(String rel, String type, URI uri)
        {
            this.links.put(rel, some(type), uri);
            return (T) this;
        }
        
        @SuppressWarnings("unchecked")
        public T addLinkTemplate(String rel, String template)
        {
            this.links.putTemplate(rel, template);
            return (T) this;
        }
    }
    
    public static abstract class UnsafeBuilderWithLinks<A, T extends UnsafeBuilderWithLinks<A, T>>
        extends BuilderWithLinks<T> implements UnsafeBuilder<A>
    {
        protected abstract A buildUnsafe();
        
        public A build() throws InvalidModelException
        {
            try
            {
                return buildUnsafe();
            }
            catch (SchemaViolationException e)
            {
                throw modelException(e);
            }
        }
    }
    
    public static class AddonBuilder extends UnsafeBuilderWithLinks<Addon, AddonBuilder>
    {
        private String name;
        private String key;
        private AddonStatus status;
        private Option<String> summary = none();
        private Option<String> tagLine = none();
        private Option<AddonVersion> version = none();
        private Option<Boolean> enableAtlassianAnswers = none();
        private Map<String, URI> externalLinks = new HashMap<String, URI>();
        private Option<Boolean> storesPersonalData;

        private AddonBuilder()
        {
            links.put("alternate", TestModelBuilders.DEFAULT_URI);
        }
        
        private AddonBuilder(Addon from)
        {
            links.put(from.getLinks());
            name = from.getName();
            key = from.getKey();
            status = from.getStatus();
            summary = from.getSummary();
            tagLine = from.getTagLine();
            version = from.getVersion();
            enableAtlassianAnswers = from.isEnableAtlassianAnswers();
            for (AddonExternalLinkType type: AddonExternalLinkType.values())
            {
                for (URI u: from.getExternalLinkUri(type))
                {
                    externalLinks.put(type.getKey(), u);
                }
            }
            storesPersonalData = from.storesPersonalData();
        }
        
        protected Addon buildUnsafe()
        {
            Addon addon = new Addon();
            addon._links = links.build();
            
            addon._embedded = new Addon.Embedded();
            addon._embedded.banner = none();
            addon._embedded.logo = none();
            addon._embedded.categories = ImmutableList.of();
            addon._embedded.distribution = addonDistributionSummary().build();
            addon._embedded.reviews = addonReviewsSummary(0, 0);
            addon._embedded.vendor = none();
            addon._embedded.version = version;
            addon._embedded = validateInstance(addon._embedded);
            
            addon.name = name;
            addon.key = key;
            addon.status = status;
            addon.summary = summary;
            addon.tagLine = tagLine;
            addon.legacy = none();
            addon.enableAtlassianAnswers = enableAtlassianAnswers;
            addon.vendorLinks = ImmutableMap.copyOf(externalLinks);
            addon.storesPersonalData = storesPersonalData;

            return validateInstance(addon);
        }
        
        /**
         * @see AddonBase#getName()
         */
        public AddonBuilder name(String name)
        {
            this.name = checkNotNull(name);
            return this;
        }
        
        /**
         * @see AddonBase#getKey()
         */
        public AddonBuilder key(String key)
        {
            this.key = checkNotNull(key);
            return this;
        }
        
        /**
         * @see AddonBase#getStatus()
         */
        public AddonBuilder status(AddonStatus status)
        {
            this.status = status;
            return this;
        }

        /**
         * @see AddonBase#getSummary()
         */
        public AddonBuilder summary(Option<String> summary)
        {
            this.summary = checkNotNull(summary);
            return this;
        }

        /**
         * @see AddonBase#getTagLine()
         */
        public AddonBuilder tagLine(Option<String> tagLine)
        {
            this.tagLine = checkNotNull(tagLine);
            return this;
        }

        /**
         * @see Addon#getBanner()
         */
        public AddonBuilder banner(Option<ImageId> image)
        {
            links.put("banner", image.map(resourceIdToUriFunc));
            return this;
        }
        
        /**
         * @see AddonBase#getLogo()
         */
        public AddonBuilder logo(Option<ImageId> image)
        {
            links.put("logo", image.map(resourceIdToUriFunc));
            return this;
        }
        
        /**
         * @see AddonBase#getCategoryIds()
         */
        public AddonBuilder categories(Iterable<AddonCategoryId> categories)
        {
            links.put("categories", StreamSupport.stream(categories.spliterator(), false)
                    .map(resourceIdToUriFunc)
                    .collect(toList()));
            return this;
        }

        /**
         * Specifies the add-on's vendor using a {@link VendorSummary} instance.
         * @see #vendor(VendorId)
         */
        public AddonBuilder vendor(VendorSummary vendor)
        {
            return vendor(vendor.getId());
        }

        /**
         * @see AddonBase#getVendorId()
         * @see VendorBase#getId()
         * @see #vendor(VendorSummary) 
         */
        public AddonBuilder vendor(VendorId id)
        {
            links.put("vendor", id.getUri());
            return this;
        }

        /**
         * @see Addon#isEnableAtlassianAnswers()
         */
        public AddonBuilder enableAtlassianAnswers(boolean enableAtlassianAnswers)
        {
            this.enableAtlassianAnswers = some(enableAtlassianAnswers);
            return this;
        }
        
        /**
         * Attaches an {@link AddonVersion} to the {@link Addon}.  You must do this if you are
         * creating a new add-on, since an add-on cannot be created with no versions.
         * @see Addon#getVersion()
         */
        public AddonBuilder version(Option<AddonVersion> version)
        {
            this.version = checkNotNull(version);
            return this;
        }
        
        /**
         * Sets one of the vendor-specified external links for the add-on.
         * @param type an {@link AddonExternalLinkType}
         * @param uri the optional link URI
         * @see Addon#getExternalLinkUri(AddonExternalLinkType)
         */
        public AddonBuilder externalLinkUri(AddonExternalLinkType type, Option<URI> uri)
        {
            if (!type.canSetForNewAddons())
            {
                throw new IllegalArgumentException("Cannot set " + type + " link for new add-ons");
            }
            externalLinks.remove(type.getKey());
            for (URI u: uri)
            {
                externalLinks.put(type.getKey(), u);
            }
            return this;
        }

        /**
         * @see Addon#storesPersonalData()
         */
        public AddonBuilder storesPersonalData(Option<Boolean> storesPersonalData) {
            this.storesPersonalData = checkNotNull(storesPersonalData);
            return this;
        }
    }
    
    public static class AddonVersionBuilder extends UnsafeBuilderWithLinks<AddonVersion, AddonVersionBuilder>
    {
        private Long buildNumber;
        private Option<Long> dataCenterBuildNumber = none();
        private Option<String> name = none();
        private AddonVersionStatus status;
        private PaymentModel paymentModel;
        private Option<URI> agreement = none();
        private Option<ImmutableList<Highlight>> highlights = none();
        private Option<ImmutableList<Screenshot>> screenshots = none();
        private Option<String> youtubeId = none();
        private Option<ImmutableList<VersionCompatibility>> compatibilities = none();
        private java.time.LocalDate releaseDate;
        private Option<String> releasedBy = none();
        private boolean beta = false;
        private boolean staticAddon = false;
        private boolean supported = false;
        private boolean deployable = false;
        private Option<String> releaseSummary = none();
        private Option<HtmlString> moreDetails = none();
        private Option<HtmlString> releaseNotes = none();
        private Map<String, URI> externalLinks = new HashMap<String, URI>();
        
        private AddonVersionBuilder()
        {
        }
        
        private AddonVersionBuilder(AddonVersion from)
        {
            links.put(from.getLinks());
            buildNumber = from.getBuildNumber();
            dataCenterBuildNumber = from.getDataCenterBuildNumber();
            name = from.getName();
            status = from.getStatus();
            paymentModel = from.getPaymentModel();
            releaseDate = from.getLocalReleaseDate();
            releasedBy = from.getReleasedBy();
            highlights = copyOptionalList(from.getHighlightsIfSpecified());
            screenshots = copyOptionalList(from.getScreenshotsIfSpecified());
            youtubeId = from.getYoutubeId();
            compatibilities = copyOptionalList(from.getCompatibilitiesIfSpecified());
            beta = from.isBeta();
            supported = from.isSupported();
            staticAddon = from.isStatic();
            deployable = from.isDeployable();
            releaseSummary = from.getReleaseSummary();
            moreDetails = from.getMoreDetails();
            releaseNotes = from.getReleaseNotes();
            for (AddonVersionExternalLinkType type: AddonVersionExternalLinkType.values())
            {
                for (URI u: from.getExternalLinkUri(type))
                {
                    externalLinks.put(type.getKey(), u);
                }
            }
        }
        
        protected AddonVersion buildUnsafe()
        {
            AddonVersion ret = new AddonVersion();
            ret._links = links.put("agreement", agreement).build();
            ret._embedded = new AddonVersion.Embedded();
            ret._embedded.artifact = none();
            ret._embedded.functionalCategories = ImmutableList.of();
            ret._embedded.highlights = highlights;
            ret._embedded.license = none();
            ret._embedded.screenshots = screenshots;
            ret.buildNumber = buildNumber;
            ret.dataCenterBuildNumber = dataCenterBuildNumber;
            ret.name = name;
            ret.status = status;
            ret.paymentModel = paymentModel;
            ret.youtubeId = youtubeId;
            ret.compatibilities = compatibilities;
            ret.staticAddon = staticAddon;
            ret.deployable = deployable;
            ret.deployment = new AddonVersion.DeploymentProperties();
            ret.deployment.autoUpdateAllowed = false;
            ret.deployment.cloud = false;
            ret.deployment.connect = false;
            ret.deployment.dataCenter = false;
            ret.deployment.dataCenterStatus = none();
            ret.deployment.permissions = none();
            ret.deployment.server = false;
            ret.legacy = none();
            AddonVersionBase.ReleaseProperties release = new AddonVersionBase.ReleaseProperties();
            release.beta = beta;
            release.date = releaseDate;
            release.releasedBy = releasedBy;
            release.supported = supported;
            ret.release = validateInstance(release);
            ret.text = new AddonVersion.TextProperties();
            ret.text.releaseSummary = releaseSummary;
            ret.text.moreDetails = moreDetails;
            ret.text.releaseNotes = releaseNotes;
            ret.text = validateInstance(ret.text);
            ret.vendorLinks = ImmutableMap.copyOf(externalLinks);
            return validateInstance(ret);
        }
        
        /**
         * @see AddonVersionBase#getArtifactInfo()
         * @see AddonVersionBase#getArtifactUri()
         */
        public AddonVersionBuilder artifact(Option<ArtifactId> artifact)
        {
            links.put("artifact", artifact.map(resourceIdToUriFunc));
            return this;
        }
        
        /**
         * Specifies the version's build number, a value that distinguishes it from all other versions
         * of the add-on and determines the correct ordering of versions.  You must specify this for
         * all new versions-- although in the case of an Atlassian Connect add-on, the value you set
         * is arbitrary since Marketplace will replace it with an automatically computed value.  The
         * build number cannot be changed after a version is created.
         * @see AddonVersion#getBuildNumber()
         */
        public AddonVersionBuilder buildNumber(long buildNumber)
        {
            this.buildNumber = buildNumber;
            return this;
        }

        /**
         * Specifies the version's Data Center build number, a value that distinguishes it from all other versions
         * of the add-on and determines the correct ordering of versions. It is optional -- only required for add-ons
         * that support Data Center versions of products.
         *
         * @see AddonVersion#getDataCenterBuildNumber()
         */
        public AddonVersionBuilder dataCenterBuildNumber(@Nullable Long dataCenterBuildNumber) {
            this.dataCenterBuildNumber = option(dataCenterBuildNumber);
            return this;
        }

        /**
         * Specifies the version name (a.k.a. version string), such as "1.0".  You can omit this if you
         * are creating a new version that is either 1. an installable jar/obr (since Marketplace can
         * find the version string from its {@code atlassian-plugin.xml}) or 2. an Atlassian Connect
         * plugin (since Marketplace automatically generates version strings for these).
         * @see AddonVersionBase#getName()
         */
        public AddonVersionBuilder name(String name)
        {
            this.name = some(name);
            return this;
        }
        
        /**
         * Provide a link to the current Marketplace agreement when creating a version, 
         * unless the agreement was already accepted for a previous version.
         * you can find the agreement at http://www.atlassian.com/licensing/marketplace/publisheragreement
         */
        public AddonVersionBuilder agreement(URI agreement)
        {
            this.agreement = some(agreement);
            return this;
        }
        
        /**
         * @see AddonVersionBase#getStatus()
         */
        public AddonVersionBuilder status(AddonVersionStatus status)
        {
            this.status = checkNotNull(status);
            return this;
        }

        /**
         * @see AddonVersion#getLicenseType()
         */
        public AddonVersionBuilder licenseType(Option<LicenseType> licenseType)
        {
            links.put("license", licenseType.flatMap(EntityFunctions.selfUri()));
            return this;
        }

        /**
         * @see AddonVersion#getLicenseTypeId()
         */
        public AddonVersionBuilder licenseTypeId(Option<LicenseTypeId> licenseTypeId)
        {
            links.put("license", licenseTypeId.map(resourceIdToUriFunc));
            return this;
        }
        
        /**
         * @see AddonVersionBase#getPaymentModel()
         */
        public AddonVersionBuilder paymentModel(PaymentModel paymentModel)
        {
            this.paymentModel = checkNotNull(paymentModel);
            return this;
        }

        public AddonVersionBuilder releaseDate(java.time.LocalDate releaseDate)
        {
            this.releaseDate = checkNotNull(releaseDate);
            return this;
        }

        /**
         * @see AddonVersionBase#getReleasedBy()
         */
        public AddonVersionBuilder releasedBy(Option<String> releasedBy)
        {
            this.releasedBy = checkNotNull(releasedBy);
            return this;
        }
        
        /**
         * @see AddonVersion#getHighlights()
         */
        public AddonVersionBuilder highlights(Iterable<Highlight> highlights)
        {
            this.highlights = some(ImmutableList.copyOf(highlights));
            return this;
        }
        
        /**
         * @see AddonVersion#getScreenshots()
         */
        public AddonVersionBuilder screenshots(Iterable<Screenshot> screenshots)
        {
            this.screenshots = some(ImmutableList.copyOf(screenshots));
            return this;
        }
        
        /**
         * @see AddonVersion#getYoutubeId()
         */
        public AddonVersionBuilder youtubeId(Option<String> youtubeId)
        {
            this.youtubeId = youtubeId;
            return this;
        }
        
        /**
         * @see AddonVersion#getCompatibilities()
         */
        public AddonVersionBuilder compatibilities(Iterable<VersionCompatibility> compatibilities)
        {
            this.compatibilities = some(ImmutableList.copyOf(compatibilities));
            return this;
        }

        /**
         * @see AddonVersionBase#isBeta()
         */
        public AddonVersionBuilder beta(boolean beta)
        {
            this.beta = beta;
            return this;
        }
        
        /**
         * @see AddonVersionBase#isSupported()
         */
        public AddonVersionBuilder supported(boolean supported)
        {
            this.supported = supported;
            return this;
        }
        
        /**
         * @see AddonVersionBase#isStatic()
         */
        public AddonVersionBuilder staticAddon(boolean staticAddon)
        {
            this.staticAddon = staticAddon;
            return this;
        }

        /**
         * @see AddonVersionBase#isDeployable()
         */
        public AddonVersionBuilder deployable(boolean deployable)
        {
            this.deployable = deployable;
            return this;
        }

        /**
         * @see AddonVersion#getReleaseSummary()
         */
        public AddonVersionBuilder releaseSummary(Option<String> releaseSummary)
        {
            this.releaseSummary = releaseSummary;
            return this;
        }
        
        /**
         * @see AddonVersion#getMoreDetails()
         */
        public AddonVersionBuilder moreDetails(Option<HtmlString> moreDetails)
        {
            this.moreDetails = moreDetails;
            return this;
        }
        
        /**
         * @see AddonVersion#getReleaseNotes()
         */
        public AddonVersionBuilder releaseNotes(Option<HtmlString> releaseNotes)
        {
            this.releaseNotes = releaseNotes;
            return this;
        }

        /**
         * Sets one of the vendor-specified external links for the add-on version.
         * @param type an {@link AddonVersionExternalLinkType}
         * @param uri the optional link URI
         * @see AddonVersion#getExternalLinkUri(AddonVersionExternalLinkType)
         */
        public AddonVersionBuilder externalLinkUri(AddonVersionExternalLinkType type, Option<URI> uri)
        {
            if (!type.canSetForNewAddonVersions())
            {
                throw new IllegalArgumentException("Cannot set " + type + " link for new add-ons");
            }
            externalLinks.remove(type.getKey());
            for (URI u: uri)
            {
                externalLinks.put(type.getKey(), u);
            }
            return this;
        }
    }
    
    public static class AddressBuilder implements UnsafeBuilder<Address>
    {
        private String line1;
        private Option<String> line2 = none();
        private Option<String> city = none();
        private Option<String> state = none();
        private Option<String> postCode = none();
        private Option<String> country = none();
        
        public Address build() throws InvalidModelException
        {
            try
            {
                Address ret = new Address();
                ret.line1 = line1;
                ret.line2 = line2;
                ret.city = city;
                ret.state = state;
                ret.postCode = postCode;
                ret.country = country;
                return validateInstance(ret);
            }
            catch (SchemaViolationException e)
            {
                throw modelException(e);
            }
        }
        
        /**
         * @see Address#getLine1()
         */
        public AddressBuilder line1(String line1)
        {
            this.line1 = checkNotNull(line1);
            return this;
        }
        
        /**
         * @see Address#getLine2()
         */
        public AddressBuilder line2(Option<String> line2)
        {
            this.line2 = checkNotNull(line2);
            return this;
        }
        
        /**
         * @see Address#getCity()
         */
        public AddressBuilder city(Option<String> city)
        {
            this.city = checkNotNull(city);
            return this;
        }
        
        /**
         * @see Address#getState()
         */
        public AddressBuilder state(Option<String> state)
        {
            this.state = checkNotNull(state);
            return this;
        }
        
        /**
         * @see Address#getPostCode()
         */
        public AddressBuilder postCode(Option<String> postCode)
        {
            this.postCode = checkNotNull(postCode);
            return this;
        }
        
        /**
         * @see Address#getCountry()
         */
        public AddressBuilder country(Option<String> country)
        {
            this.country = checkNotNull(country);
            return this;
        }
    }
    
    public static class ApplicationVersionBuilder extends UnsafeBuilderWithLinks<ApplicationVersion, ApplicationVersionBuilder>
    {
        private Integer buildNumber;
        private String name;
        private java.time.LocalDate releaseDate;
        private ApplicationVersionStatus status;
        private boolean dataCenterCompatible;
        
        private ApplicationVersionBuilder()
        {
        }
        
        private ApplicationVersionBuilder(ApplicationVersion from)
        {
            links.put(from.getLinks());
            buildNumber = from.getBuildNumber();
            name = from.getName();
            releaseDate = from.getLocalReleaseDate();
            status = from.getStatus();
            dataCenterCompatible = from.isDataCenterCompatible();
        }
        
        protected ApplicationVersion buildUnsafe()
        {
            ApplicationVersion ret = new ApplicationVersion();
            ret._links = links.build();
            ret.buildNumber = buildNumber;
            ret.version = name;
            ret.releaseDate = releaseDate;
            ret.status = status;
            ret.dataCenterCompatible = dataCenterCompatible;
            return validateInstance(ret);
        }
        
        /**
         * @see ApplicationVersion#getBuildNumber()
         */
        public ApplicationVersionBuilder buildNumber(int buildNumber)
        {
            this.buildNumber = buildNumber;
            return this;
        }
        
        /**
         * @see ApplicationVersion#getName()
         */
        public ApplicationVersionBuilder name(String name)
        {
            this.name = checkNotNull(name);
            return this;
        }

        public ApplicationVersionBuilder releaseDate(java.time.LocalDate releaseDate)
        {
            this.releaseDate = checkNotNull(releaseDate);
            return this;
        }

        /**
         * @see ApplicationVersion#getStatus()
         */
        public ApplicationVersionBuilder status(ApplicationVersionStatus status)
        {
            this.status = checkNotNull(status);
            return this;
        }

        /**
         * @see ApplicationVersion#isDataCenterCompatible()
         */
        public ApplicationVersionBuilder dataCenterCompatible(boolean dataCenterCompatible)
        {
            this.dataCenterCompatible = dataCenterCompatible;
            return this;
        }
    }
    
    public static class HighlightBuilder extends UnsafeBuilderWithLinks<Highlight, HighlightBuilder>
    {
        private ImageInfo fullImage;
        private ImageInfo thumbnailImage;
        private String title;
        private HtmlString body;
        private Option<String> explanation = none();
        
        protected Highlight buildUnsafe()
        {
            Highlight ret = new Highlight();
            ret._embedded = new Highlight.Embedded();
            ret._embedded.screenshot = fullImage;
            ret._embedded.thumbnail = thumbnailImage;
            ret._embedded = validateInstance(ret._embedded);
            ret._links = links.build();
            ret.body = body;
            ret.title = title;
            ret.explanation = explanation;
            return validateInstance(ret);
        }
        
        /**
         * @see Highlight#getFullImage()
         */
        public HighlightBuilder fullImage(ImageId fullImage)
        {
            links.put("screenshot", fullImage.getUri());
            this.fullImage = imageInfo().build();
            return this;
        }

        /**
         * @see Highlight#getThumbnailImage()
         */
        public HighlightBuilder thumbnailImage(ImageId thumbnailImage)
        {
            links.put("thumbnail", thumbnailImage.getUri());
            this.thumbnailImage = imageInfo().build();
            return this;
        }

        /**
         * @see Highlight#getTitle()
         */
        public HighlightBuilder title(String title)
        {
            this.title = checkNotNull(title);
            return this;
        }

        /**
         * @see Highlight#getBody()
         */
        public HighlightBuilder body(HtmlString body)
        {
            this.body = checkNotNull(body);
            return this;
        }

        /**
         * @see Highlight#getExplanation()
         */
        public HighlightBuilder explanation(Option<String> explanation)
        {
            this.explanation = checkNotNull(explanation);
            return this;
        }
    }
    
    public static class LinksBuilder implements SafeBuilder<Links>
    {
        private Map<String, List<Link>> links = new HashMap<>();

        public Links build()
        {
            return new Links(links);
        }

        /**
         * Adds a regular resource link, replacing any other links with the same key.
         */
        public LinksBuilder put(String rel, URI uri)
        {
            return put(rel, none(String.class), uri);
        }

        /**
         * Adds a list of regular resource links for a single link rel, replacing any other links with
         * the same key.
         */
        public LinksBuilder put(String rel, Iterable<URI> uris)
        {

            return put(rel, StreamSupport.stream(uris.spliterator(), false)
                    .map(uri -> Link.fromUri(uri, none(String.class)))
                    .collect(toList()));
        }

        /**
         * Adds a link with an optional content type, replacing any other links with the same key.
         */
        public LinksBuilder put(String rel, Option<String> type, URI uri)
        {
            return put(rel, Arrays.asList(Link.fromUri(uri, type)));
        }

        /**
         * Optionally adds a resource link, making no change if it is {@link Option#none()}.
         */
        public LinksBuilder put(String rel, Option<URI> maybeUri)
        {
            for (URI uri: maybeUri)
            {
                return put(rel, uri);
            }
            return this;
        }
        
        /**
         * Adds a list of links of any kind, replacing any other links with the same key.
         */
        public LinksBuilder put(String rel, List<Link> values)
        {
            links.put(rel, ImmutableList.copyOf(values));
            return this;
        }

        /**
         * Copies all the links from another {@link Links} object, replacing any links with the same
         * key.
         */
        public LinksBuilder put(Links from)
        {
            links.putAll(from.getItemsMap());
            return this;
        }
        
        /**
         * Adds a link template, replacing any other links with the same key.
         */
        public LinksBuilder putTemplate(String rel, String template)
        {
            return put(rel, Arrays.asList(Link.fromUriTemplate(UriTemplate.create(template), none(String.class))));
        }
        
        /**
         * Removes any links with the specified key.
         */
        public LinksBuilder remove(String rel)
        {
            links.remove(rel);
            return this;
        }
        
        /**
         * Removes all links.
         */
        public LinksBuilder removeAll()
        {
            links.clear();
            return this;
        }
    }
    
    public static class ScreenshotBuilder extends UnsafeBuilderWithLinks<Screenshot, ScreenshotBuilder>
    {
        private ImageInfo image;
        private Option<String> caption = none();

        protected Screenshot buildUnsafe()
        {
            Screenshot ret = new Screenshot();
            ret._links = links.build();
            ret._embedded = new ScreenshotEmbedded();
            ret._embedded.image = image;
            ret._embedded = validateInstance(ret._embedded);
            ret.caption = caption;
            return validateInstance(ret);
        }

        /**
         * @see Screenshot#getImage()
         */
        public ScreenshotBuilder image(ImageId image)
        {
            links.put("image", image.getUri());
            this.image = imageInfo().build();
            return this;
        }
        
        /**
         * @see Screenshot#getCaption()
         */
        public ScreenshotBuilder caption(Option<String> caption)
        {
            this.caption = checkNotNull(caption);
            return this;
        }
    }

    public static class VersionCompatibilityBuilder implements SafeBuilder<VersionCompatibility> {
        private ApplicationKey applicationKey;
        private Option<CompatibilityHostingBounds> server = none();
        private Option<CompatibilityHostingBounds> dataCenter = none();
        private Option<Boolean> cloud = none();

        public VersionCompatibilityBuilder(VersionCompatibility from) {
            applicationKey = from.getApplication();
            server = from.getHosting().getServer();
            dataCenter = from.getHosting().getDataCenter();
            cloud = some(from.isCloudCompatible());
        }

        @Override
        public VersionCompatibility build() {
            return new VersionCompatibility(applicationKey, new CompatibilityHosting(server, dataCenter, cloud));
        }

        public VersionCompatibilityBuilder setCloud(@Nullable Boolean cloud) {
            this.cloud = option(cloud);
            return this;
        }

        public VersionCompatibilityBuilder setServer(int min, int max) {
            server = some(makeHostingBounds(min, max));
            return this;
        }

        public VersionCompatibilityBuilder setDataCenter(int min, int max) {
            dataCenter = some(makeHostingBounds(min, max));
            return this;
        }
    }

    public static class VendorBuilder extends UnsafeBuilderWithLinks<Vendor, VendorBuilder>
    {
        private String name;
        private Option<String> description = none();
        private String email;
        private Option<Address> address = none();
        private Option<String> phone = none();
        private Option<String> otherContactDetails = none();
        private Map<String, URI> externalLinks = new HashMap<>();
        private SupportDetails supportDetails = new SupportDetails();
        private VendorPrograms programs;

        private VendorBuilder()
        {
            links.put("alternate", DEFAULT_URI);
            programs = new VendorPrograms();
        }
        
        private VendorBuilder(Vendor from)
        {
            links.put(from.getLinks());
            name = from.getName();
            description = from.getDescription();
            address = from.getAddress();
            email = from.getEmail();
            phone = from.getPhone();
            otherContactDetails = from.getOtherContactDetails();
            for (VendorExternalLinkType type: VendorExternalLinkType.values())
            {
                for (URI u: from.getExternalLinkUri(type))
                {
                    externalLinks.put(type.getKey(), u);
                }
            }
            supportDetails = from.getSupportDetails();
            programs = from.getPrograms();
        }
        
        protected Vendor buildUnsafe()
        {
            Vendor ret = new Vendor();
            ret._links = links.build();
            ret._embedded = new Vendor.Embedded();
            ret._embedded.logo = none();
            ret.address = address;
            ret.description = description;
            ret.email = email;
            ret.name = name;
            ret.otherContactDetails = otherContactDetails;
            ret.phone = phone;
            ret.vendorLinks = ImmutableMap.copyOf(externalLinks);
            ret.verifiedStatus = none();
            ret.isAtlassian = none();
            ret.supportDetails = supportDetails;
            ret.programs = programs;
            return validateInstance(ret);
        }
        
        /**
         * @see Vendor#getLogo()
         */
        public VendorBuilder logo(Option<ImageId> logo)
        {
            links.put("logo", logo.map(resourceIdToUriFunc));
            return this;
        }

        /**
         * @see Vendor#getName()
         */
        public VendorBuilder name(String name)
        {
            this.name = checkNotNull(name);
            return this;
        }

        /**
         * @see Vendor#getDescription()
         */
        public VendorBuilder description(Option<String> description)
        {
            this.description = checkNotNull(description);
            return this;
        }

        /**
         * @see Vendor#getAddress()
         */
        public VendorBuilder address(Option<Address> address)
        {
            this.address = checkNotNull(address);
            return this;
        }
        
        /**
         * @see Vendor#getEmail()
         */
        public VendorBuilder email(String email)
        {
            this.email = checkNotNull(email);
            return this;
        }
        
        /**
         * @see Vendor#getPhone()
         */
        public VendorBuilder phone(Option<String> phone)
        {
            this.phone = checkNotNull(phone);
            return this;
        }

        /**
         * @see Vendor#getOtherContactDetails()
         */
        public VendorBuilder otherContactDetails(Option<String> otherContactDetails)
        {
            this.otherContactDetails = checkNotNull(otherContactDetails);
            return this;
        }

        /**
         * @see Vendor#getSupportDetails()
         */
        public VendorBuilder supportDetails(SupportDetails supportDetails)
        {
            this.supportDetails = supportDetails;
            return this;
        }

        /**
         * @see Vendor#getPrograms()
         */
        public VendorBuilder programs(VendorPrograms programs) {
            this.programs = programs;
            return this;
        }

        /**
         * Sets one of the vendor-specified external links for the vendor.
         * @param type a {@link VendorExternalLinkType}
         * @param uri the optional link URI
         * @see Vendor#getExternalLinkUri(VendorExternalLinkType)
         */
        public VendorBuilder externalLinkUri(VendorExternalLinkType type, Option<URI> uri)
        {
            externalLinks.remove(type.getKey());
            for (URI u: uri)
            {
                externalLinks.put(type.getKey(), u);
            }
            return this;
        }
    }

    private static <T> Option<ImmutableList<T>> copyOptionalList(Option<Iterable<T>> list)
    {
        for (Iterable<T> l: list)
        {
            return some(ImmutableList.copyOf(l));
        }
        return none();
    }
}
