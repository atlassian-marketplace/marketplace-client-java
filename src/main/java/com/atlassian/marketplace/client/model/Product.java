package com.atlassian.marketplace.client.model;

import io.atlassian.fugue.Option;

import java.net.URI;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

/**
 * Information about an Atlassian software package, such as JIRA Service Desk.
 * @see com.atlassian.marketplace.client.api.Products
 * @since 2.0.0
 */
public final class Product
{
    Links _links;
    Embedded _embedded;
    String key;
    String name;
    String summary;

    public Links getLinks()
    {
        return _links;
    }

    /**
     * The short unique identifier for the product.
     */
    public String getKey()
    {
        return key;
    }

    /**
     * The product name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * A human-readable short description of the product.
     */
    public String getSummary()
    {
        return summary;
    }
    
    /**
     * The URI of the web page describing available downloads for this product, if any.
     */
    public Option<URI> getDownloadsPageUri()
    {
        return _links.getUri("downloads");
    }

    /**
     * The product logo.
     */
    public Option<ImageInfo> getLogo()
    {
        return _embedded.logo;
    }

    /**
     * The alternate logo that includes the product name as part of the image.
     */
    public Option<ImageInfo> getTitleLogo()
    {
        return _embedded.titleLogo;
    }
    
    /**
     * The product's current version (or the version that matched the query), if available.
     * Note that some product properties, such as the URI of the installable binary, are really
     * properties of the version and will only be available if you set the "withVersion" property
     * on the query.
     */
    public Option<ProductVersion> getVersion()
    {
        return _embedded.version;
    }
    
    /**
     * The version string of the product's current version (or the version that matched the query),
     * if available. Same as calling {@link #getVersion()} and then calling {@link ProductVersion#getName()}
     * on the result.
     */
    public Option<String> getVersionName()
    {
        for (ProductVersion v: getVersion())
        {
            return some(v.getName());
        }
        return none();
    }
    
    static final class Embedded
    {
        Option<ImageInfo> logo;
        Option<ImageInfo> titleLogo;
        Option<ProductVersion> version;
    }
}
