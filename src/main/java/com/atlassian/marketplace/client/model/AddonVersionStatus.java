package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates whether an {@link AddonVersion} is publicly visible, private, or pending approval.
 * @since 2.0.0
 */
public enum AddonVersionStatus implements EnumWithKey
{
    /**
     * The version is visible to everyone.
     */
    PUBLIC("public"),
    /**
     * The version is visible only to users associated with its vendor; it can also be installed
     * by anyone who has received an access token from the vendor.
     */
    PRIVATE("private"),
    /**
     * The version is pending approval, and will become public once it has been approved.
     */
    SUBMITTED("submitted");
    
    private final String key;
    
    AddonVersionStatus(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
